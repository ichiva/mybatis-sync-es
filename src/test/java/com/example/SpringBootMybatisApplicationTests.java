package com.example;

import com.gzwl.SpringBootApplication;
import com.gzwl.dao.EmployeeMapper;
import com.gzwl.entity.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest(classes = SpringBootApplication.class)
class SpringBootMybatisApplicationTests {


    @Autowired
    EmployeeMapper employeeMapper;

    @Test
    @Transactional
    void contextLoads() {
        Employee employee = new Employee();
        employee.setEmployeeLoginName("周杰伦");
        employee.setEmployeeNickName("Jayson");
        employee.setEmployeeLoginPassword("123456");

        int id = employeeMapper.insert(employee);
        System.out.println("受影响行数="+ id);
    }


    @Test
    void last() {
        Employee employee = employeeMapper.lastEmployee();
        System.out.println(employee);
    }

}
