package com.gzwl.interceptor;

/**
 * 同步es异常
 */
public class SyncEsException extends RuntimeException {

    public SyncEsException(){}

    public SyncEsException(Throwable e){
        super(e);
    }

}
