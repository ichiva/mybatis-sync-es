package com.gzwl.interceptor;

/**
 * es 同步处理器
 */
public interface SynEsHandler {

    void handler(Object parameter);
}
