package com.gzwl.interceptor.impl;

import com.gzwl.entity.Employee;
import com.gzwl.interceptor.SynEsHandler;
import com.gzwl.interceptor.SyncEsInterceptor;
import org.apache.ibatis.binding.MapperMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class EmployeeMapperHandler implements SynEsHandler {

    @Autowired
    public EmployeeMapperHandler(SyncEsInterceptor syncEsInterceptor){
        //将自己注册到拦截器中,每个方法可用是独立的handler，也可用注册多个方法的处理器
        syncEsInterceptor.regHandler("com.gzwl.dao.EmployeeMapper.insert",this);
        syncEsInterceptor.regHandler("com.gzwl.dao.EmployeeMapper.update",this);
    }

    @Override
    public void handler(Object parameter) {
        Employee entity;

        if(parameter instanceof Employee){
            //insert 可用直接拦截到实体类
            entity = (Employee) parameter;
        }else {
            //update 方法需要特殊处理
            entity = (Employee) ((MapperMethod.ParamMap) parameter).get("param1");
        }

        //在这里编写es的同步逻辑
    }
}
