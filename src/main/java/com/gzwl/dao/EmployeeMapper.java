package com.gzwl.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzwl.entity.Employee;

public interface EmployeeMapper extends BaseMapper<Employee> {

    Employee lastEmployee();
}
