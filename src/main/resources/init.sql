CREATE TABLE `employee` (
	`employee_id` INT(11) NOT NULL AUTO_INCREMENT,
	`employee_login_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_persian_ci',
	`employee_login_password` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8_persian_ci',
	`employee_nick_name` VARCHAR(50) NULL DEFAULT NULL,
	`role_id` INT(11) NULL DEFAULT NULL,
	`department_id` INT(11) NULL DEFAULT NULL,
	`job_id` INT(11) NULL DEFAULT NULL,
	`employee_status` BIT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`employee_id`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=17
;
